//
//  NetworkingTests.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-05.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import XCTest
@testable import CityWeather

class NetworkingTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testLocalWeatherAPI() {
        let place = "Vancouver"
        let days = 5
        let action = WorldWeatherAPI.Router.LocalWeather(place, days)
        let request = action.URLRequest
        guard let requestURL = request.URL else {
            XCTFail("poorly constructed URLRequest")
            return
        }

        let expectedURL = "https://api.worldweatheronline.com/premium/v1/weather.ashx"
        // swiftlint:disable:next force_unwrapping
        let actualURL = "\(requestURL.scheme)://\(requestURL.host!)\(requestURL.path!)"
        XCTAssertEqual(actualURL, expectedURL, "LocalWeather endpoint incorrect")

        let expectedItems = [
            "format": "json",
            "key": "a6cf3137ed3140e599d185557160206",
            "q": place,
            "num_of_days": String(days),
        ]
        var actualItems = [String: String]()
        let queryItems = NSURLComponents(URL: requestURL, resolvingAgainstBaseURL: false)?.queryItems
        queryItems?.forEach { actualItems[$0.name] = $0.value }
        XCTAssertEqual(expectedItems, actualItems, "LocalWeather parameters incorrect")
    }

    func testLocationSearchAPI() {
        let query = "Vancouver"
        let results = 25
        let action = WorldWeatherAPI.Router.LocationSearch(query, results)
        let request = action.URLRequest
        guard let requestURL = request.URL else {
            XCTFail("poorly constructed URLRequest")
            return
        }

        let expectedURL = "https://api.worldweatheronline.com/premium/v1/search.ashx"
        // swiftlint:disable:next force_unwrapping
        let actualURL = "\(requestURL.scheme)://\(requestURL.host!)\(requestURL.path!)"
        XCTAssertEqual(actualURL, expectedURL, "LocationSearch endpoint incorrect")

        let expectedItems = [
            "format": "json",
            "key": "a6cf3137ed3140e599d185557160206",
            "query": query,
            "num_of_results": String(results),
            ]
        var actualItems = [String: String]()
        let queryItems = NSURLComponents(URL: requestURL, resolvingAgainstBaseURL: false)?.queryItems
        queryItems?.forEach { actualItems[$0.name] = $0.value }
        XCTAssertEqual(expectedItems, actualItems, "LocationSearch parameters incorrect")
    }
}

//
//  DayViewController.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-06.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import Crashlytics

class DayViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var topLeftLabel: UILabel!
    @IBOutlet weak var topRightLabel: UILabel!
    @IBOutlet weak var midLeftLabel: UILabel!
    @IBOutlet weak var midRightLabel: UILabel!
    @IBOutlet weak var botLeftLabel: UILabel!
    @IBOutlet weak var botRightLabel: UILabel!

    // MARK: - View model

    var viewModel: DayViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }

            viewModel.didUpdate = { [weak self] in
                self?.viewModelDidUpdate()
            }

            viewModelDidUpdate()
        }
    }

    // MARK: - Life cycle

    func viewModelDidUpdate() {
        guard let _ = backgroundImage, vm = viewModel else {
            return
        }

        topLeftLabel.text = vm.dateLabel
        topRightLabel.text = nil
        midLeftLabel.text = vm.uvLabel
        midRightLabel.text = vm.sunriseLabel
        botLeftLabel.text = vm.temperatureLabel
        botRightLabel.text = vm.sunsetLabel
        if let imageURL = vm.imageURL {
            backgroundImage.af_setImageWithURL(imageURL)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModelDidUpdate()

        Answers.logContentViewWithName(String(self.dynamicType), contentType: nil, contentId: nil, customAttributes: nil)
    }
}

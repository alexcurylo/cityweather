//
//  MasterViewController.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-03.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import Crashlytics
import SVProgressHUD
import ActionSheetPicker_3_0

/**
 View of the list of locations weather can be retrieved for
 */
final class MasterViewController: UITableViewController {

    // MARK: - Properties

    var detailViewController: DetailViewController?
    var detailIndex = 0

    // MARK: - View model

    var viewModel: MasterViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }

            viewModel.didUpdate = { [weak self] in
                self?.viewModelDidUpdate()
            }
            viewModel.didError = { [weak self] (error) in
                self?.viewModelDidError(error)
            }
            viewModel.willSelect = { [weak self] (strings, select) in
                self?.viewModelWillSelect(strings, select: select)
            }

            viewModelDidUpdate()
        }
    }

    func viewModelDidUpdate() {
        if let tableView = tableView {
            tableView.reloadData()
        }

        if let detailViewController = detailViewController {
            // Note that this will display first in list (if any) on iPhone 6+ at startup. Seems reasonable.
            detailViewController.viewModel = viewModel?.detailViewModel(atIndex: detailIndex)
        }
    }

    func viewModelDidError(error: NSError) {
        let title = R.string.localizable.errorTitle()
        let okTitle = R.string.localizable.oK()
        let message = error.localizedDescription
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: okTitle, style: .Default, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }

    func viewModelWillSelect(strings: [String], select: (String) -> Void) {
        ActionSheetStringPicker.showPickerWithTitle(R.string.localizable.addSelect(),
            rows: strings,
            initialSelection: 0,
            doneBlock: { picker, index, value in
                select(value as? String ?? "")
                return
            }, cancelBlock: { ActionStringCancelBlock in
            }, origin: view)
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(addCity(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as? UINavigationController)?.topViewController as? DetailViewController
        }

        Answers.logContentViewWithName(String(self.dynamicType), contentType: nil, contentId: nil, customAttributes: nil)
    }

    override func viewWillAppear(animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController?.collapsed ?? true
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)

        // If there's no cities, presumably they would like one
        if let viewModel = viewModel where viewModel.cityCount == 0 {
            addCity(self)
        }
    }

    // MARK: - Actions

    func addCity(sender: AnyObject) {
        let title = R.string.localizable.addTitle()
        let message = R.string.localizable.addMessage()
        let cancelTitle = R.string.localizable.cancel()
        let okTitle = R.string.localizable.oK()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: okTitle, style: .Default, handler: { action in
            let city = alert.textFields?[0].text ?? ""
            guard !city.isEmpty else {
                return
            }

            SVProgressHUD.show()
            self.viewModel?.addCity(named: city) { [weak self] result in
                SVProgressHUD.dismiss()
                switch result {
                case .Failure(let error):
                    self?.viewModelDidError(error)
                default:
                    break
                }
            }

            Answers.logCustomEventWithName("CityAdded", customAttributes: ["city": city])
        }))
        alert.addTextFieldWithConfigurationHandler({ textField in })
        presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let typedInfo = R.segue.masterViewController.showDetail(segue: segue),
                indexPath = tableView.indexPathForSelectedRow,
                controller = typedInfo.destinationViewController.topViewController as? DetailViewController else {
            return
        }

        detailViewController = controller
        detailIndex = indexPath.row
        detailViewController?.viewModel = viewModel?.detailViewModel(atIndex: detailIndex)
        detailViewController?.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem()
        detailViewController?.navigationItem.leftItemsSupplementBackButton = true
    }

    // MARK: - Table View

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.cityCount ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.cell, forIndexPath: indexPath) ?? UITableViewCell()

        let label = viewModel?.cityName(atIndex: indexPath.row) ?? ""
        configureCell(cell, withLabel: label)

        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            viewModel?.deleteCity(atIndex: indexPath.row)
        }
    }

    func configureCell(cell: UITableViewCell, withLabel label: String) {
        cell.textLabel?.text = label
    }
}

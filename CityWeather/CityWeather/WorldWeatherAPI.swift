//
//  WorldWeatherAPI.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-05.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

/**
 Wraps World Weather API

 - seealso: [World Weather API documentation](http://sdkbridge.com/weather/index.html?getting_started.htm)
 */
struct WorldWeatherAPI {

    /// Definitions for unexpected results
    static let ErrorKey = "error"
    static let ErrorMessageKey = "msg"
    static let ErrorDomain = "WorldWeatherAPI"

    /// for building Alamofire requests
    enum Router: URLRequestConvertible {

        /// common to all endpoints
        static let baseURLString = "https://api.worldweatheronline.com/premium/v1/"
        static let commonParameters = [
            "format": "json",
            "key": "a6cf3137ed3140e599d185557160206",
        ]

        case LocalWeather(String, Int)

        case LocationSearch(String, Int)

        /* ???: Placeholders for complete API support
        case MarineWeather

        case PastWeather

        case TimeZone
         */

        /// provide the request
        var URLRequest: NSMutableURLRequest {
            guard let baseUrl = NSURL(string: Router.baseURLString) else {
                fatalError("Base URL could not form a URL.")
            }

            var parameters = Router.commonParameters
            let endpoint: String
            switch self {
            case LocalWeather(let place, let days):
                endpoint = "weather.ashx"
                parameters["q"] = place
                parameters["num_of_days"] = String(days)
            case LocationSearch(let query, let results):
                endpoint = "search.ashx"
                parameters["query"] = query
                parameters["num_of_results"] = String(results)
            }

            let request = NSMutableURLRequest(URL: baseUrl.URLByAppendingPathComponent(endpoint))
            let encoding = Alamofire.ParameterEncoding.URL
            return encoding.encode(request, parameters: parameters).0
        }
    }

    /**
     Parses a response for error state

     - parameter json: API result

     - returns: NSError for error state of nil
     */
    static func errorFrom(json: JSON) -> NSError? {

        guard let data = json[LocalWeather.DataKey].dictionary,
            error = data[ErrorKey]?.array else {
                return nil
        }
        guard !error.isEmpty,
            let first = error[0].dictionary,
            message = first[ErrorMessageKey]?.string where !message.isEmpty else {
                let localized = R.string.localizable.errorUnexpected()
                return NSError(domain: ErrorDomain, code: 1, userInfo: [NSLocalizedDescriptionKey: localized])
        }

        return NSError(domain: ErrorDomain, code: 1, userInfo: [NSLocalizedDescriptionKey: message])
    }
}

//
//  Month.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Month: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let AbsMaxTempFKey = "absMaxTemp_F"
    static let AbsMaxTempKey = "absMaxTemp"
    static let AvgMinTempFKey = "avgMinTemp_F"
    static let AvgMinTempKey = "avgMinTemp"
	static let IndexKey = "index"
    static let NameKey = "name"


    // MARK: Properties
    @NSManaged var climateAverages: ClimateAverages

    @NSManaged var absMaxTempF: String
    @NSManaged var absMaxTemp: String
    @NSManaged var avgMinTempF: String
	@NSManaged var avgMinTemp: String
	@NSManaged var index: String
    @NSManaged var name: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        avgMinTempF = json[Month.AvgMinTempFKey].stringValue
        avgMinTemp = json[Month.AvgMinTempKey].stringValue
        absMaxTempF = json[Month.AbsMaxTempFKey].stringValue
		absMaxTemp = json[Month.AbsMaxTempKey].stringValue
		index = json[Month.IndexKey].stringValue
        name = json[Month.NameKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[Month.AbsMaxTempFKey] = absMaxTempF
        dictionary[Month.AbsMaxTempKey] = absMaxTemp
        dictionary[Month.AvgMinTempFKey] = avgMinTempF
        dictionary[Month.AvgMinTempKey] = avgMinTemp
        dictionary[Month.IndexKey] = index
        dictionary[Month.NameKey] = name

        return dictionary
    }
}

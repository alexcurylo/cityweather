//
//  Hourly.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Hourly: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let ChanceoffogKey = "chanceoffog"
    static let ChanceoffrostKey = "chanceoffrost"
    static let ChanceofhightempKey = "chanceofhightemp"
    static let ChanceofovercastKey = "chanceofovercast"
    static let ChanceofrainKey = "chanceofrain"
    static let ChanceofremdryKey = "chanceofremdry"
    static let ChanceofsnowKey = "chanceofsnow"
    static let ChanceofsunshineKey = "chanceofsunshine"
    static let ChanceofthunderKey = "chanceofthunder"
    static let ChanceofwindyKey = "chanceofwindy"
    static let CloudcoverKey = "cloudcover"
    static let DewPointCKey = "DewPointC"
    static let DewPointFKey = "DewPointF"
    static let FeelsLikeCKey = "FeelsLikeC"
    static let FeelsLikeFKey = "FeelsLikeF"
    static let HeatIndexCKey = "HeatIndexC"
    static let HeatIndexFKey = "HeatIndexF"
    static let HumidityKey = "humidity"
    static let PrecipMMKey = "precipMM"
    static let PressureKey = "pressure"
    static let TempCKey = "tempC"
    static let TempFKey = "tempF"
    static let TimeKey = "time"
    static let VisibilityKey = "visibility"
    static let WeatherCodeKey = "weatherCode"
    static let WeatherDescKey = "weatherDesc"
    static let WeatherIconUrlKey = "weatherIconUrl"
    static let WindChillCKey = "WindChillC"
    static let WindChillFKey = "WindChillF"
    static let Winddir16PointKey = "winddir16Point"
    static let WinddirDegreeKey = "winddirDegree"
    static let WindGustKmphKey = "WindGustKmph"
    static let WindGustMilesKey = "WindGustMiles"
    static let WindspeedKmphKey = "windspeedKmph"
    static let WindspeedMilesKey = "windspeedMiles"


    // MARK: Properties
    @NSManaged var weather: Weather

    @NSManaged var chanceoffog: String
    @NSManaged var chanceoffrost: String
    @NSManaged var chanceofhightemp: String
    @NSManaged var chanceofovercast: String
    @NSManaged var chanceofrain: String
    @NSManaged var chanceofremdry: String
    @NSManaged var chanceofsnow: String
    @NSManaged var chanceofsunshine: String
    @NSManaged var chanceofthunder: String
    @NSManaged var chanceofwindy: String
    @NSManaged var cloudcover: String
    @NSManaged var dewPointC: String
    @NSManaged var dewPointF: String
    @NSManaged var feelsLikeC: String
    @NSManaged var feelsLikeF: String
    @NSManaged var heatIndexC: String
    @NSManaged var heatIndexF: String
    @NSManaged var humidity: String
    @NSManaged var precipMM: String
    @NSManaged var pressure: String
    @NSManaged var tempC: String
    @NSManaged var tempF: String
    @NSManaged var time: String
    @NSManaged var visibility: String
    @NSManaged var weatherCode: String
    @NSManaged var weatherDesc: NSOrderedSet
    @NSManaged var weatherIconUrl: NSOrderedSet
    @NSManaged var windChillC: String
    @NSManaged var windChillF: String
    @NSManaged var winddir16Point: String
    @NSManaged var winddirDegree: String
    @NSManaged var windGustKmph: String
    @NSManaged var windGustMiles: String
    @NSManaged var windspeedKmph: String
    @NSManaged var windspeedMiles: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
// swiftlint:disable:previous function_body_length
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        chanceoffog = json[Hourly.ChanceoffogKey].stringValue
        chanceoffrost = json[Hourly.ChanceoffrostKey].stringValue
        chanceofhightemp = json[Hourly.ChanceofhightempKey].stringValue
        chanceofovercast = json[Hourly.ChanceofovercastKey].stringValue
        chanceofrain = json[Hourly.ChanceofrainKey].stringValue
        chanceofremdry = json[Hourly.ChanceofremdryKey].stringValue
        chanceofsnow = json[Hourly.ChanceofsnowKey].stringValue
        chanceofsunshine = json[Hourly.ChanceofsunshineKey].stringValue
        chanceofthunder = json[Hourly.ChanceofthunderKey].stringValue
        chanceofwindy = json[Hourly.ChanceofwindyKey].stringValue
        cloudcover = json[Hourly.CloudcoverKey].stringValue
        dewPointC = json[Hourly.DewPointCKey].stringValue
        dewPointF = json[Hourly.DewPointFKey].stringValue
        feelsLikeC = json[Hourly.FeelsLikeCKey].stringValue
        feelsLikeF = json[Hourly.FeelsLikeFKey].stringValue
        heatIndexC = json[Hourly.HeatIndexCKey].stringValue
        heatIndexF = json[Hourly.HeatIndexFKey].stringValue
        humidity = json[Hourly.HumidityKey].stringValue
        precipMM = json[Hourly.PrecipMMKey].stringValue
        pressure = json[Hourly.PressureKey].stringValue
        tempC = json[Hourly.TempCKey].stringValue
        tempF = json[Hourly.TempFKey].stringValue
        time = json[Hourly.TimeKey].stringValue
        visibility = json[Hourly.VisibilityKey].stringValue
        weatherCode = json[Hourly.WeatherCodeKey].stringValue
        var temp = NSMutableOrderedSet()
        for item in json[Hourly.WeatherDescKey].arrayValue {
            let model = WeatherDesc(json: item, context: context)
            model.hourly = self
            temp.addObject(model)
        }
        weatherDesc = temp
        temp = []
        for item in json[Hourly.WeatherIconUrlKey].arrayValue {
            let model = WeatherIconUrl(json: item, context: context)
            model.hourly = self
            temp.addObject(model)
        }
        weatherIconUrl = temp
        windChillC = json[Hourly.WindChillCKey].stringValue
        windChillF = json[Hourly.WindChillFKey].stringValue
        winddir16Point = json[Hourly.Winddir16PointKey].stringValue
        winddirDegree = json[Hourly.WinddirDegreeKey].stringValue
        windGustKmph = json[Hourly.WindGustKmphKey].stringValue
        windGustMiles = json[Hourly.WindGustMilesKey].stringValue
        windspeedKmph = json[Hourly.WindspeedKmphKey].stringValue
        windspeedMiles = json[Hourly.WindspeedMilesKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
// swiftlint:disable:previous function_body_length
        var dictionary = [String: AnyObject]()

        dictionary[Hourly.ChanceoffogKey] = chanceoffog
        dictionary[Hourly.ChanceoffrostKey] = chanceoffrost
        dictionary[Hourly.ChanceofhightempKey] = chanceofhightemp
        dictionary[Hourly.ChanceofovercastKey] = chanceofovercast
        dictionary[Hourly.ChanceofrainKey] = chanceofrain
        dictionary[Hourly.ChanceofremdryKey] = chanceofremdry
        dictionary[Hourly.ChanceofsnowKey] = chanceofsnow
        dictionary[Hourly.ChanceofsunshineKey] = chanceofsunshine
        dictionary[Hourly.ChanceofthunderKey] = chanceofthunder
        dictionary[Hourly.ChanceofwindyKey] = chanceofwindy
        dictionary[Hourly.CloudcoverKey] = cloudcover
        dictionary[Hourly.DewPointCKey] = dewPointC
        dictionary[Hourly.DewPointFKey] = dewPointF
        dictionary[Hourly.FeelsLikeCKey] = feelsLikeC
        dictionary[Hourly.FeelsLikeFKey] = feelsLikeF
        dictionary[Hourly.HeatIndexCKey] = heatIndexC
        dictionary[Hourly.HeatIndexFKey] = heatIndexF
        dictionary[Hourly.HumidityKey] = humidity
        dictionary[Hourly.PrecipMMKey] = precipMM
        dictionary[Hourly.PressureKey] = pressure
        dictionary[Hourly.TempCKey] = tempC
        dictionary[Hourly.TempFKey] = tempF
        dictionary[Hourly.TimeKey] = time
        dictionary[Hourly.VisibilityKey] = visibility
        dictionary[Hourly.WeatherCodeKey] = weatherCode
        var temp = [AnyObject]()
        for item in weatherDesc {
            if let item = item as? WeatherDesc {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Hourly.WeatherDescKey] = temp
        temp = []
        for item in weatherIconUrl {
            if let item = item as? WeatherIconUrl {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Hourly.WeatherIconUrlKey] = temp
        dictionary[Hourly.WindChillCKey] = windChillC
        dictionary[Hourly.WindChillFKey] = windChillF
        dictionary[Hourly.Winddir16PointKey] = winddir16Point
        dictionary[Hourly.WinddirDegreeKey] = winddirDegree
        dictionary[windGustKmph] = windGustKmph
        dictionary[Hourly.WindGustMilesKey] = windGustMiles
        dictionary[Hourly.WindspeedKmphKey] = windspeedKmph
        dictionary[Hourly.WindspeedMilesKey] = windspeedMiles

        return dictionary
    }
}

//
//  DayViewModel.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-06.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit

/**
 View model of a particular day forecast for a specified city
 */
class DayViewModel: AnyObject {

    // MARK: - Model injections

    private(set) var weather: [String: AnyObject]
    private(set) var hours: [[String: AnyObject]]?
    private(set) var astronomy: [[String: AnyObject]]?

    init(weather: [String: AnyObject]) {
        self.weather = weather
        self.hours = weather[Weather.HourlyKey] as? [[String: AnyObject]]
        self.astronomy = weather[Weather.AstronomyKey] as? [[String: AnyObject]]
    }

    // MARK: - View callbacks

    var didUpdate: (() -> Void)? = nil

    // MARK: - View accessors

    var dateLabel: String {
        guard let date = weather[Weather.DateKey] as? String else {
            return ""
        }
        return "🗓\(date)"
    }

    var temperatureLabel: String {
        guard let low = weather[Weather.MintempCKey] else {
            return ""
        }
        var result = "🌡\(low)°C"
        if let high = weather[Weather.MaxtempCKey] {
            result += " - \(high)°C"
        }
        return result
    }

    var uvLabel: String {
        guard let uv = weather[Weather.UvIndexKey] as? String else {
            return ""
        }
        return "😎\(uv) UV"
    }

    var sunriseLabel: String {
        guard let sunrise = astronomy?[0][Astronomy.SunriseKey] as? String else {
            return ""
        }
        return "🌅\(sunrise)"
    }

    var sunsetLabel: String {
        guard let sunset = astronomy?[0][Astronomy.SunsetKey] as? String else {
            return ""
        }
        return "🌇\(sunset)"
    }

    var imageURL: NSURL? {
        guard let firstIcon = (hours?[4][Hourly.WeatherIconUrlKey] as? [[String: AnyObject]])?[0],
            iconUrl = firstIcon[WeatherUrl.ValueKey] as? String else {
                return NSURL(string: "http://placekitten.com/500/500")
        }

        return NSURL(string: iconUrl)
    }
}

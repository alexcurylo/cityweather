[![Language](http://img.shields.io/badge/language-swift_2.2.1-brightgreen.svg?style=flat)](https://developer.apple.com/swift)
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat)](http://mit-license.org)

[cityweather](https://bitbucket.org/alexcurylo/cityweather)
=============

An iPhone weather forecast application for user-designated cities.

## Table of Contents

1. [Purpose](#purpose)
2. [Requirements](#requirements)
3. [Usage](#usage)
4. [Design Choices](#design-choices)
5. [Approaches](#approaches)
6. [Libraries](#libraries)
7. [Roadmap](#roadmap)
8. [Author](#author)
9. [License](#license)
10. [Change Log](#change-log)

## Purpose

Technical demo of building a contemporary weatehr forecasting app.

## Requirements

- Xcode 7.3.1 or later
- iOS 9.3 or later
- A [Bitbucket](https://bitbucket.org) account, to contribute or file issues at [the project repo](https://bitbucket.org/alexcurylo/cityweather)
- (Optional) [SwiftLint](https://github.com/realm/SwiftLint), to keep source style opinionated

## Usage

At startup, a table of previously retrieved cities (if any) is presented. Tap on one to display any saved forecast and retrieve the latest for the next 5 days.
 
On the detail screen (or right detail pane, on an iPhone 6+/6S+ in landscape mode) current conditions for the selected city are displayed in the top header and the 5 days are in a swipeable paging controller below that. The background images of the header and controller are drawn from URLs provided by the API and represent conditions for that day. (Or, if you find a location that doesn't report a valid URL, you'll see a kitten instead. Because kitten!)
 
New cities may be added with the add (+) button on top right
 
Cities may be removed by activating edit mode by the (Edit) button on top left, or by swiping, as is conventional for iOS table views.
 
![Image of 6S+](Screenshot.jpeg)

## Design Choices
 
The architecture separates
 - Networking
 - Data Store (Model)
 - Presenter (View Model)
 - View
In common parlance, we draw from the MVVM and MVC-N architectural patterns.

## Approaches
  
The architecture chosen was the simplest consistent with foreseeable project scope expansion and responsibility separation, with dependency injection the preferred form of interface for improved testability and implementation changes.
 
Separating networking from the model enables offline operation and allows for prepopulated data, versioning, and alternate sources.
 
Separating the view from the model via the presenter allows for arbitrary model changes and for arbitrary display redesign and/or alternative representations with a minimum of friction.
 
Mirroring all available information from the API allows for future enhancements without data model migrations and allows us to use automated tools to aid the modelling process.

## Libraries

Cocoapods is the generally accepted standard for iOS dependency management, and Crashlytics/Answers/Beta are familiar to most developers from wide usage, so Fabric is a natural choice for development crash reporting/analytics/distribution.
 
The use of stringly typed code is an abomination birthed by Satan Himself for the torment of debugging developers, therefore we use [R.swift](https://github.com/mac-cain13/R.swift/) and [R.swift.Library](https://github.com/mac-cain13/R.swift.Library) for Swiftily typed resource identifiers.
 
For the Core Data model layer, we chose [JSQCoreDataKit](http://www.jessesquires.com/JSQCoreDataKit/) from the multitude of available options for managing boilerplate and/or adding functionality because a) it's the most well-roundedly Swifty-feeling helper we know of, and b) we're massively intrigued by the latest release [dropping the main as the background's child](https://github.com/jessesquires/JSQCoreDataKit/releases/tag/4.0.0) pattern that's been popular the last several years, so we wanted to check that out, and this is a good excuse.
 
For the networking layer, [AlamoFire](https://github.com/Alamofire/Alamofire) is the generally accepted standard for HTTP communication management in Swift, and [AlamofireSwiftyJSON](https://github.com/starboychina/AlamofireSwiftyJSON/blob/master/Source/AlamofireSwiftyJSON.swift) adds elegance to interfacing it and [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) the most popular of the multitude of Swift JSON wrappers out there.
 
To mirror the JSON structure into our data layer, for speed and accuracy we use [JSON Accelerator](https://itunes.apple.com/in/app/json-accelerator/id511324989?mt=12&at=10l4B9) to produce the Core Data model; but as it doesn't output Swift, we use [SwiftyJSONAccelerator](https://github.com/insanoid/SwiftyJSONAccelerator) to provide Swift-idiomatic and SwiftyJSON-friendly model classes. With substantial editing required to fix mistakes and bring them in line with our preferred idioms, but hey they're a good start.
 
To enhance the UI presentation, specifically loading available images for weather conditions, we use [AlamofireImage](https://github.com/Alamofire/AlamofireImage) as it integrates with our already chosen networking libraries to add that feature without a great deal of effort of overhead.
 
To enforce a predictable application state, we modally suspend event processing while a user-initated action is being attended to. [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD) is a popular library for that kind of thing.

To enable user selection from a list of strings, we use [ActionSheetPicker-3.0](https://cocoapods.org/pods/ActionSheetPicker-3.0) as that saves time compared to writing our own version, which would have no discernible advantage.
 
## Roadmap
 
Tthe most important feature to add would be to add a way of accessing the hourly forecasts attached to the daily forecasts. Since that would involve simply extending the existing architecture, and presumably we're not being graded on our design skills, that doesn't seem necessary.
 
Then we'd see about adding functionality from the rest of the currently used APIs and adding the other World Weather APIs, currently noted as comments in WorldWeatherAPI.swift.'
 
Some thought could also be given to adding timed refresh so that the Cities table could always display a tagline of current conditions. That would probably be activated from Settings as users are likely to not appreciate data usage without explicit or implicit approval. At least, we do.
 
Finally, we could add more explicit reachability handling. Currently failures are silent except for user-initiated operations, which will display an AFNetworking-provided message. However, we consider that adequate for any reasonable use case encountered with this app.

## Author

[![web: everywhs.com](http://img.shields.io/badge/web-www.everywhs.com-green.svg?style=flat)](http://everywhs.com) 
[![twitter: @trollwerks](http://img.shields.io/badge/twitter-%40trollwerks-blue.svg?style=flat)](https://twitter.com/trollwerks) 
[![email: alex@trollwerks.com](http://img.shields.io/badge/email-alex%40trollwerks.com-orange.svg?style=flat)](mailto:alex@trollwerks.com) 

## License

The [MIT License](http://opensource.org/licenses/MIT). See the [LICENSE](LICENSE) file for details.
 
## Change Log
 
 * **Version 1.0**: *(2016.06.07)* - Initial Release


//
//  NetworkController.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-05.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit
import Alamofire


/**
 Results of a network operation request

 - Active: Duplicate of active request
 - Available: unexpired previous results available
 - Invalid: Unacceptable parameters provided
 - Success: Succeeded, results available in model
 - Failure: NSError with localized description is available
 */
enum NetworkResult {

    case Active
    case Available
    case Invalid
    case Success
    case Failure(NSError)

    func error() -> NSError? {
        if case .Failure(let error) = self {
            return error
        }
        return nil
    }
}
typealias NetworkCompletion = (NetworkResult) -> Void

/**
 Encapsulate networking operations
 */
class NetworkController: AnyObject {

    // MARK: - Model injections

    let stack: CoreDataStack

    // MARK: - Properties

    var locationOperations = Set<String>()
    var weatherOperations = Set<String>()

    init(stack: CoreDataStack) {
        self.stack = stack
    }

    // MARK: - View Model accessors

    /**
     Search for locations matching a query pattern

     - parameter query: pattern to search for
     - parameter completion: NetworkCompletion called on completion
     */
    func locationSearch(query: String, completion: NetworkCompletion? = nil) {
        guard !query.isEmpty else {
            completion?(.Invalid)
            return
        }
        // these shouldn't overlap, but safety first
        guard !locationOperations.contains(query) else {
            completion?(.Active)
            return
        }

        // Don't query more than every 24 hours
        let expiry: NSTimeInterval = 24 * 60 * 60
        let childContext = stack.childContext()
        // Clean expired retrieval(s), return on unexpired
        let existing = existingQueries(query, context: childContext)
        for exists in existing {
            let interval = NSDate().timeIntervalSinceDate(exists.timestamp)
            guard interval > expiry else {
                completion?(.Available)
                return
            }
        }

        locationOperations.insert(query)
        let action = WorldWeatherAPI.Router.LocationSearch(query, 25)
        Alamofire.request(action).validate().responseSwiftyJSON(
            options: .AllowFragments,
            completionHandler: { (request, response, json, error) in
                self.locationOperations.remove(query)

                if let error = error as? NSError {
                    print("locationSearch error \(error)")
                    completion?(.Failure(error))
                } else {
                    existing.forEach { childContext.deleteObject($0) }

                    if let error = WorldWeatherAPI.errorFrom(json) {
                        self.save(childContext)
                        completion?(.Failure(error))
                        return
                    }

                    let locationSearch = LocationSearch(json: json, context:childContext)
                    locationSearch.query = query
                    locationSearch.timestamp = NSDate()
                    self.save(childContext)
                    completion?(.Success)
                }
        })
    }

    /**
     Get all available information for the specified city

     - parameter city: City name
     - parameter completion: NetworkCompletion called on completion
     */
    func getLocalWeather(city: String, completion: NetworkCompletion? = nil) {
        guard !city.isEmpty else {
            completion?(.Invalid)
            return
        }
        // Don't query while an operation's in progress
        guard !weatherOperations.contains(city) else {
            completion?(.Active)
            return
        }

        // Don't query more than every 5 minutes
        let expiry: NSTimeInterval = 5 * 60
        let childContext = stack.childContext()
        // Clean expired retrieval(s), return on unexpired
        let existing = existingCities(city, context: childContext)
        for exists in existing {
            let interval = NSDate().timeIntervalSinceDate(exists.timestamp)
            guard interval > expiry else {
                completion?(.Available)
                return
            }
        }

        // ok, update
        weatherOperations.insert(city)
        let action = WorldWeatherAPI.Router.LocalWeather(city, 5)
        Alamofire.request(action).validate().responseSwiftyJSON(
            options: .AllowFragments,
            completionHandler: { (request, response, json, error) in
                self.weatherOperations.remove(city)

                if let error = error as? NSError {
                    print("getLocalWeather error \(error)")
                    completion?(.Failure(error))
                } else {
                    existing.forEach { childContext.deleteObject($0) }

                    if let error = WorldWeatherAPI.errorFrom(json) {
                        self.save(childContext)
                        completion?(.Failure(error))
                        return
                    }

                    let localWeather = LocalWeather(json: json, context: childContext)
                    localWeather.city = city
                    localWeather.timestamp = NSDate()
                    self.save(childContext)
                    completion?(.Success)
                }
            })
    }

    // MARK: - Core Data Saving support

    func existingCities(city: String, context: NSManagedObjectContext) -> [LocalWeather] {
        var existing = [LocalWeather]()
        let lwEntity = entity(name: String(LocalWeather), context: context)
        let fetchRequest = FetchRequest<LocalWeather>(entity: lwEntity)
        fetchRequest.fetchBatchSize = 20
        let predicate = NSPredicate(format:"city LIKE %@", city)
        fetchRequest.predicate = predicate
        do {
            existing = try context.fetch(request: fetchRequest)
        } catch {
            // Surprising but not fatal in release builds
            assertionFailure("FETCH FAIL: \(error)")
        }
        return existing
    }

    func existingQueries(query: String, context: NSManagedObjectContext) -> [LocationSearch] {
        var existing = [LocationSearch]()
        let lsEntity = entity(name: String(LocationSearch), context: context)
        let fetchRequest = FetchRequest<LocationSearch>(entity: lsEntity)
        fetchRequest.fetchBatchSize = 20
        let predicate = NSPredicate(format:"query LIKE %@", query)
        fetchRequest.predicate = predicate
        do {
            existing = try context.fetch(request: fetchRequest)
        } catch {
            // Surprising but not fatal in release builds
            assertionFailure("FETCH FAIL: \(error)")
        }
        return existing
    }

    func save(context: NSManagedObjectContext) {
        saveContext(context) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                // Surprising but not fatal in release builds
                assertionFailure("Failed to save context: \(error)")
            }
        }
    }
}

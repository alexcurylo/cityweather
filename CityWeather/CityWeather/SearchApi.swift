//
//  SearchApi.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class SearchApi: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let ResultKey = "result"


    // MARK: Properties
    @NSManaged var locationSearch: LocationSearch

    @NSManaged var result: NSOrderedSet


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        let temp = NSMutableOrderedSet()
        for item in json[SearchApi.ResultKey].arrayValue {
            let model = Result(json: item, context: context)
            model.searchApi = self
            temp.addObject(model)
        }
        result = temp
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

            var temp = [AnyObject]()
			for item in result {
                if let item = item as? Result {
				temp.append(item.dictionaryRepresentation())
			}
            dictionary[SearchApi.ResultKey] = temp
		}

        return dictionary
    }
}

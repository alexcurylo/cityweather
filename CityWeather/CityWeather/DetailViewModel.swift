//
//  DetailViewModel.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-06.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit

/**
 View model of the 5 day forecast for a specified city
 */
class DetailViewModel: AnyObject {

    // MARK: - Model injections

    private(set) var localWeather: [String: AnyObject]
    private(set) var data: [String: AnyObject]?
    private(set) var current: [String: AnyObject]?
    private(set) var weather: [[String: AnyObject]]?

    init(localWeather: [String: AnyObject]) {
        self.localWeather = localWeather
        self.data = localWeather[LocalWeather.DataKey] as? [String: AnyObject]
        self.current = (data?[Data.CurrentConditionKey] as? [[String: AnyObject]])?[0]
        self.weather = data?[Data.WeatherKey] as? [[String: AnyObject]]
    }

    // MARK: - View callbacks

    var didUpdate: (() -> Void)? = nil

    // MARK: - View accessors

    var cityLabel: String {
        guard let city = localWeather[LocalWeather.CityKey] as? String else {
            return ""
        }
        let cityCountry = city.componentsSeparatedByString(",")
        return cityCountry[0]
    }

    var timeLabel: String {
        guard let time = current?[CurrentCondition.ObservationTimeKey] else {
            return ""
        }
        return "🕰\(time)"
    }

    var descriptionLabel: String {
        let descsArray = current?[CurrentCondition.WeatherDescKey] as? [[String: AnyObject]]
        guard let descs = descsArray where !descs.isEmpty,
            let desc = descs[0][WeatherDesc.ValueKey] as? String else {
                return ""
        }
        return "🔮\(desc)"
    }

    var temperatureLabel: String {
        guard let temp = current?[CurrentCondition.TempCKey] else {
            return ""
        }
        var result = "🌡\(temp)°C"
        if let feels = current?[CurrentCondition.FeelsLikeCKey] {
            result += " (😰\(feels)°C)"
        }
        return result
    }

    var windLabel: String {
        guard let speed = current?[CurrentCondition.WindspeedKmphKey] else {
            return ""
        }
        var result = "💨\(speed)K"
        if let degree = current?[CurrentCondition.WinddirDegreeKey] {
            result += " 🚩\(degree)°"
        }
        return result
    }

    var rainLabel: String {
        guard let rain = current?[CurrentCondition.PrecipMMKey] else {
            return ""
        }
        var result = "☔️\(rain)mm"
        if let cloud = current?[CurrentCondition.CloudcoverKey] {
            result += " ☁️\(cloud)%"
        }
        return result
    }

    var imageURL: NSURL? {
        let iconsArray = current?[CurrentCondition.WeatherIconUrlKey] as? [[String: AnyObject]]
        guard let icons = iconsArray where !icons.isEmpty,
            let iconUrl = icons[0][WeatherUrl.ValueKey] as? String else {
                return NSURL(string: "http://placekitten.com/320/220")
        }
        return NSURL(string: iconUrl)
    }

    func dayViewModel(atIndex index: Int) -> DayViewModel {
        guard let weather = weather where weather.count > index else {
            return DayViewModel(weather: [:])
        }

        return DayViewModel(weather: weather[index])
    }
}

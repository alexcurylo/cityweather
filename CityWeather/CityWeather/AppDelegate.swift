//
//  AppDelegate.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-03.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import JSQCoreDataKit
import SVProgressHUD

/**
 Singleton UIApplication delegate
 */
@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    var master: MasterViewController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        // Stand up crash reporting/analytics
        Fabric.with([Crashlytics.self])

        // Set up our custom font usage
        customizeUI()

        // Locate required UI elements
        if let splitViewController = window?.rootViewController as? UISplitViewController,
            navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as? UINavigationController {
            navigationController.topViewController?.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
            splitViewController.delegate = self

            if let masterNavigationController = splitViewController.viewControllers[0] as? UINavigationController {
                master = masterNavigationController.topViewController as? MasterViewController
            }
        }

        // Stand up data model
        createCoreDataStack()

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        save()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        save()
    }

    // MARK: - Customization

    func customizeUI() {
        guard let titleFont =  UIFont(name: "ProximaNovaSoft-Bold", size: 24),
            buttonFont = UIFont(name: "ProximaNovaSoft-Semibold", size: 18)  else {
            return
        }

        var navTitleAttributes = UINavigationBar.appearance().titleTextAttributes ?? [:]
        navTitleAttributes[NSFontAttributeName] = titleFont
        //navTitleAttributes[NSForegroundColorAttributeName] = UIColor.purpleColor()
        UINavigationBar.appearance().titleTextAttributes = navTitleAttributes

        var barButtonTitleAttributes = UIBarButtonItem.appearance().titleTextAttributesForState(.Normal) ?? [:]
        barButtonTitleAttributes[NSFontAttributeName] = buttonFont
        UIBarButtonItem.appearance().setTitleTextAttributes(barButtonTitleAttributes, forState: .Normal)

        SVProgressHUD.setDefaultMaskType(.Black)
    }

    // MARK: - Split view

    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        if topAsDetailController.backgroundImage == nil {
            // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
            return true
        }
        return false
    }

    // MARK: - Core Data stack

    var stack: CoreDataStack? {
        didSet {
            guard let stack = stack, master = master else {
                return
            }
            let network = NetworkController(stack: stack)
            let viewModel = MasterViewModel(stack: stack, network: network)
            master.viewModel = viewModel
        }
    }

    func createCoreDataStack() {
        let model = CoreDataModel(name: "CityWeather", bundle: _R.hostingBundle)
        if model.needsMigration {
            do {
                try model.migrate()
            } catch {
                // Embarassing but not fatal in release builds
                assertionFailure("Failed to migrate model: \(error)")
                do {
                    try model.removeExistingStore()
                } catch let error as NSError {
                    print("removeExistingStore error \(error)")
                }
            }
        }

        let factory = CoreDataStackFactory(model: model)
        factory.createStack(onQueue: nil) { result in
            switch result {
            case .success(let newStack):
                self.stack = newStack
            case .failure(let error):
                // App is non-functional. Crash.
                fatalError("Failed to create stack: \(error)")
            }
        }
    }

    // MARK: - Core Data Saving support

    func save() {
        guard let context = stack?.mainContext else {
            return
        }

        saveContext(context) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                // Surprising but not fatal in release builds
                assertionFailure("Failed to save context: \(error)")
            }
        }
    }
}

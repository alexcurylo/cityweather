//
//  LocationSearch.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class LocationSearch: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let SearchApiKey = "search_api"

    static let QueryKey = "query"
    static let TimestampKey = "timestamp"


    // MARK: Properties
	@NSManaged var searchApi: SearchApi

    @NSManaged var query: String
    @NSManaged var timestamp: NSDate


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        searchApi = SearchApi(json: json[LocationSearch.SearchApiKey], context: context)
        searchApi.locationSearch = self
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[LocationSearch.SearchApiKey] = searchApi.dictionaryRepresentation()
        dictionary[LocationSearch.QueryKey] = query
        dictionary[LocationSearch.TimestampKey] = timestamp

        return dictionary
    }
}

//
//  CurrentCondition.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class CurrentCondition: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let CloudcoverKey = "cloudcover"
    static let FeelsLikeCKey = "FeelsLikeC"
    static let FeelsLikeFKey = "FeelsLikeF"
    static let HumidityKey = "humidity"
    static let ObservationTimeKey = "observation_time"
    static let PrecipMMKey = "precipMM"
    static let PressureKey = "pressure"
    static let TempCKey = "temp_C"
    static let TempFKey = "temp_F"
    static let VisibilityKey = "visibility"
    static let WeatherCodeKey = "weatherCode"
    static let WeatherDescKey = "weatherDesc"
    static let WeatherIconUrlKey = "weatherIconUrl"
    static let Winddir16PointKey = "winddir16Point"
    static let WinddirDegreeKey = "winddirDegree"
    static let WindspeedKmphKey = "windspeedKmph"
    static let WindspeedMilesKey = "windspeedMiles"


    // MARK: Properties
    @NSManaged var data: Data

    @NSManaged var cloudcover: String
    @NSManaged var feelsLikeC: String
    @NSManaged var feelsLikeF: String
    @NSManaged var humidity: String
    @NSManaged var observationTime: String
    @NSManaged var precipMM: String
    @NSManaged var pressure: String
    @NSManaged var tempC: String
    @NSManaged var tempF: String
    @NSManaged var visibility: String
    @NSManaged var weatherCode: String
    @NSManaged var weatherDesc: NSOrderedSet
    @NSManaged var weatherIconUrl: NSOrderedSet
    @NSManaged var winddir16Point: String
    @NSManaged var winddirDegree: String
    @NSManaged var windspeedKmph: String
    @NSManaged var windspeedMiles: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        cloudcover = json[CurrentCondition.CloudcoverKey].stringValue
        feelsLikeC = json[CurrentCondition.FeelsLikeCKey].stringValue
        feelsLikeF = json[CurrentCondition.FeelsLikeFKey].stringValue
        humidity = json[CurrentCondition.HumidityKey].stringValue
        observationTime = json[CurrentCondition.ObservationTimeKey].stringValue
        precipMM = json[CurrentCondition.PrecipMMKey].stringValue
        pressure = json[CurrentCondition.PressureKey].stringValue
        tempC = json[CurrentCondition.TempCKey].stringValue
        tempF = json[CurrentCondition.TempFKey].stringValue
        visibility = json[CurrentCondition.VisibilityKey].stringValue
        weatherCode = json[CurrentCondition.WeatherCodeKey].stringValue
        var temp = NSMutableOrderedSet()
        for item in json[CurrentCondition.WeatherDescKey].arrayValue {
            let model = WeatherDesc(json: item, context: context)
            model.currentCondition = self
            temp.addObject(model)
        }
        weatherDesc = temp
        temp = []
        for item in json[CurrentCondition.WeatherIconUrlKey].arrayValue {
            let model = WeatherIconUrl(json: item, context: context)
            model.currentCondition = self
            temp.addObject(model)
        }
        weatherIconUrl = temp
        winddir16Point = json[CurrentCondition.Winddir16PointKey].stringValue
        winddirDegree = json[CurrentCondition.WinddirDegreeKey].stringValue
        windspeedKmph = json[CurrentCondition.WindspeedKmphKey].stringValue
        windspeedMiles = json[CurrentCondition.WindspeedMilesKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[CurrentCondition.CloudcoverKey] = cloudcover
        dictionary[CurrentCondition.FeelsLikeCKey] = feelsLikeC
        dictionary[CurrentCondition.FeelsLikeFKey] = feelsLikeF
        dictionary[CurrentCondition.HumidityKey] = humidity
        dictionary[CurrentCondition.ObservationTimeKey] = observationTime
        dictionary[CurrentCondition.PrecipMMKey] = precipMM
        dictionary[CurrentCondition.PressureKey] = pressure
        dictionary[CurrentCondition.TempCKey] = tempC
        dictionary[CurrentCondition.TempFKey] = tempF
        dictionary[CurrentCondition.VisibilityKey] = visibility
        dictionary[CurrentCondition.WeatherCodeKey] = weatherCode
        var temp = [AnyObject]()
        for item in weatherDesc {
            if let item = item as? WeatherDesc {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[CurrentCondition.WeatherDescKey] = temp
        temp = []
        for item in weatherIconUrl {
            if let item = item as? WeatherIconUrl {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[CurrentCondition.WeatherIconUrlKey] = temp
        dictionary[CurrentCondition.Winddir16PointKey] = winddir16Point
        dictionary[CurrentCondition.WinddirDegreeKey] = winddirDegree
        dictionary[CurrentCondition.WindspeedKmphKey] = windspeedKmph
        dictionary[CurrentCondition.WindspeedMilesKey] = windspeedMiles


        return dictionary
    }
}

//
//  ClimateAverages.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class ClimateAverages: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let MonthKey = "month"


    // MARK: Properties
    @NSManaged var data: Data

	@NSManaged var month: NSOrderedSet


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        let temp = NSMutableOrderedSet()
        for item in json[ClimateAverages.MonthKey].arrayValue {
            let model = Month(json: item, context: context)
            model.climateAverages = self
            temp.addObject(model)
		}
        month = temp
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        var temp = [AnyObject]()
        for item in month {
            if let item = item as? Month {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[ClimateAverages.MonthKey] = temp

        return dictionary
    }
}

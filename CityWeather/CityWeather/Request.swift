//
//  Request.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Request: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let QueryKey = "query"
	static let TypeKey = "type"


    // MARK: Properties
    @NSManaged var data: Data

	@NSManaged var query: String
	@NSManaged var type: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        query = json[Request.QueryKey].stringValue
		type = json[Request.TypeKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[Request.QueryKey] = query
        dictionary[Request.TypeKey] = type

        return dictionary
    }
}

//
//  Data.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Data: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let ClimateAveragesKey = "ClimateAverages"
    static let CurrentConditionKey = "current_condition"
	static let RequestKey = "request"
	static let WeatherKey = "weather"


    // MARK: Properties
    @NSManaged var localWeather: LocalWeather

	@NSManaged var climateAverages: NSOrderedSet
    @NSManaged var currentCondition: NSOrderedSet
	@NSManaged var request: NSOrderedSet
	@NSManaged var weather: NSOrderedSet


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

		var temp = NSMutableOrderedSet()
        for item in json[Data.ClimateAveragesKey].arrayValue {
            let model = ClimateAverages(json: item, context: context)
            model.data = self
            temp.addObject(model)
        }
        climateAverages = temp
        temp = []
        for item in json[Data.CurrentConditionKey].arrayValue {
            let model = CurrentCondition(json: item, context: context)
            model.data = self
            temp.addObject(model)
        }
        currentCondition = temp
        temp = []
        for item in json[Data.RequestKey].arrayValue {
            let model = Request(json: item, context: context)
            model.data = self
            temp.addObject(model)
		}
        request = temp
		temp = []
        for item in json[Data.WeatherKey].arrayValue {
            let model = Weather(json: item, context: context)
            model.data = self
            temp.addObject(model)
		}
        weather = temp
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        var temp = [AnyObject]()
        for item in climateAverages {
            if let item = item as? ClimateAverages {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Data.ClimateAveragesKey] = temp
        temp = []
        for item in currentCondition {
            if let item = item as? CurrentCondition {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Data.CurrentConditionKey] = temp
        temp = []
        for item in request {
            if let item = item as? Request {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Data.RequestKey] = temp
        temp = []
        for item in weather {
            if let item = item as? Weather {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Data.WeatherKey] = temp

        return dictionary
    }
}

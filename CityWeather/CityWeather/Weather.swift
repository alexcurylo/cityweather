//
//  Weather.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Weather: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let AstronomyKey = "astronomy"
    static let DateKey = "date"
    static let HourlyKey = "hourly"
    static let MaxtempCKey = "maxtempC"
    static let MaxtempFKey = "maxtempF"
    static let MintempCKey = "mintempC"
    static let MintempFKey = "mintempF"
    static let UvIndexKey = "uvIndex"


    // MARK: Properties
    @NSManaged var data: Data

    @NSManaged var astronomy: NSOrderedSet
    @NSManaged var date: String
    @NSManaged var hourly: NSOrderedSet
    @NSManaged var maxtempC: String
    @NSManaged var maxtempF: String
    @NSManaged var mintempC: String
    @NSManaged var mintempF: String
    @NSManaged var uvIndex: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        var temp = NSMutableOrderedSet()
        for item in json[Weather.AstronomyKey].arrayValue {
            let model = Astronomy(json: item, context: context)
            model.weather = self
            temp.addObject(model)
        }
        astronomy = temp
        date = json[Weather.DateKey].stringValue
        temp = []
        for item in json[Weather.HourlyKey].arrayValue {
            let model = Hourly(json: item, context: context)
            model.weather = self
            temp.addObject(model)
        }
        hourly = temp
        maxtempC = json[Weather.MaxtempCKey].stringValue
        maxtempF = json[Weather.MaxtempFKey].stringValue
        mintempC = json[Weather.MintempCKey].stringValue
        mintempF = json[Weather.MintempFKey].stringValue
        uvIndex = json[Weather.UvIndexKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        var temp = [AnyObject]()
        for item in astronomy {
            if let item = item as? Astronomy {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Weather.AstronomyKey] = temp
        dictionary[Weather.DateKey] = date
        temp = []
        for item in hourly {
            if let item = item as? Hourly {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Weather.HourlyKey] = temp
        dictionary[Weather.MaxtempCKey] = maxtempC
        dictionary[Weather.MaxtempFKey] = maxtempF
        dictionary[Weather.MintempCKey] = mintempC
        dictionary[Weather.MintempFKey] = mintempF
        dictionary[Weather.UvIndexKey] = uvIndex

        return dictionary
    }
}

//
//  Result.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Result: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let AreaNameKey = "areaName"
	static let CountryKey = "country"
    static let LatitudeKey = "latitude"
	static let LongitudeKey = "longitude"
	static let PopulationKey = "population"
	static let RegionKey = "region"
    static let WeatherUrlKey = "weatherUrl"


    // MARK: Properties
    @NSManaged var searchApi: SearchApi

    @NSManaged var areaName: NSOrderedSet
    @NSManaged var country: NSOrderedSet
	@NSManaged var latitude: String
    @NSManaged var longitude: String
	@NSManaged var population: String
	@NSManaged var region: NSOrderedSet
    @NSManaged var weatherUrl: NSOrderedSet


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        var temp = NSMutableOrderedSet()
        for item in json[Result.AreaNameKey].arrayValue {
            let model = AreaName(json: item, context: context)
            model.result = self
            temp.addObject(model)
        }
        areaName = temp
        temp = []
        for item in json[Result.CountryKey].arrayValue {
            let model = Country(json: item, context: context)
            model.result = self
            temp.addObject(model)
        }
        country = temp
        longitude = json[Result.LongitudeKey].stringValue
        latitude = json[Result.LatitudeKey].stringValue
        population = json[Result.PopulationKey].stringValue
        temp = []
        for item in json[Result.RegionKey].arrayValue {
            let model = Region(json: item, context: context)
            model.result = self
            temp.addObject(model)
        }
        region = temp
        temp = []
        for item in json[Result.WeatherUrlKey].arrayValue {
            let model = WeatherUrl(json: item, context: context)
            model.result = self
            temp.addObject(model)
        }
        weatherUrl = temp
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        var temp = [AnyObject]()
        for item in areaName {
            if let item = item as? AreaName {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Result.AreaNameKey] = temp
        temp = []
        for item in country {
            if let item = item as? Country {
                temp.append(item.dictionaryRepresentation())
            }
        }
        dictionary[Result.CountryKey] = temp
        dictionary[Result.LatitudeKey] = latitude
        dictionary[Result.LongitudeKey] = longitude
        dictionary[Result.PopulationKey] = population
        temp = []
        for item in region {
            if let item = item as? Region {
                temp.append(item.dictionaryRepresentation())
            }
        }
        temp = []
        for item in weatherUrl {
            if let item = item as? WeatherUrl {
                temp.append(item.dictionaryRepresentation())
            }
        }

        return dictionary
    }
}

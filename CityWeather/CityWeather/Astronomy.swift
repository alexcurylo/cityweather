//
//  Astronomy.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class Astronomy: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let SunriseKey = "sunrise"
	static let MoonriseKey = "moonrise"
	static let SunsetKey = "sunset"
	static let MoonsetKey = "moonset"


    // MARK: Properties
    @NSManaged var weather: Weather

    @NSManaged var sunrise: String
	@NSManaged var moonrise: String
	@NSManaged var sunset: String
	@NSManaged var moonset: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        sunrise = json[Astronomy.SunriseKey].stringValue
		moonrise = json[Astronomy.MoonriseKey].stringValue
		sunset = json[Astronomy.SunsetKey].stringValue
		moonset = json[Astronomy.MoonsetKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary.updateValue(sunrise, forKey: Astronomy.SunriseKey)
        dictionary.updateValue(moonrise, forKey: Astronomy.MoonriseKey)
        dictionary.updateValue(sunset, forKey: Astronomy.SunsetKey)
        dictionary.updateValue(moonset, forKey: Astronomy.MoonsetKey)

        return dictionary
    }
}

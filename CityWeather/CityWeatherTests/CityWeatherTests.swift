//
//  CityWeatherTests.swift
//  CityWeatherTests
//
//  Created by Alex Curylo on 2016-06-03.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import XCTest
@testable import CityWeather
import Fabric
import Crashlytics
import JSQCoreDataKit

class CityWeatherTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    /// Check that setup, target plist and main storyboard are good
    func testAppDelegateConfiguration() {
        let app = UIApplication.sharedApplication()
        let delegate = app.delegate as? AppDelegate
        XCTAssertNotNilOptional(delegate, message: "sharedApplication().delegate does not exist - set host application!")
        XCTAssertNotNilOptional(delegate?.window, message: "missing main window")
        XCTAssertNotNilOptional(delegate?.master, message: "missing master view controller")
    }

    /// check for any fatal UIApplicationDelegate side effects
    func testAppDelegateDelegation() {
        let app = UIApplication.sharedApplication()
        let delegate = app.delegate as? AppDelegate
        delegate?.applicationWillResignActive(app)
        delegate?.applicationDidEnterBackground(app)
        delegate?.applicationWillEnterForeground(app)
        delegate?.applicationDidBecomeActive(app)
        delegate?.applicationWillTerminate(app)
    }

    /// Check that resource configurations are good
    func testAppResources() {
        // R.swift provided diagnostics -- should cover stuff in storyboards
        R.assertValid()
        XCTAssertNotNilOptional(try? R.validate(), message: "Invalid resources")

        // check non-R.swift-managed resources
    }

    /// Fabric and Crashlytics library configured ok
    func testFabricCrashlytics() {
        let fabricSettings = NSBundle.mainBundle().objectForInfoDictionaryKey("Fabric") as? [String: AnyObject]
        let fabricKey = fabricSettings?["APIKey"] as? String
        XCTAssertNotNil(fabricKey, "missing Info.plist Fabric settings")
        XCTAssertFalse((fabricKey ?? "").isEmpty, "Fabric API key should not be empty")
        XCTAssertNotNil(Fabric.sharedSDK(), "missing Fabric")

        let crashlytics = Crashlytics.sharedInstance()
        XCTAssertNotNil(crashlytics, "missing Crashlytics")
        XCTAssertEqual(crashlytics.version, "3.7.0", "unexpected Crashlytics version")
        XCTAssertEqual(crashlytics.APIKey, fabricKey, "unexpected Crashlytics APIKey")
    }

    /// JSQCoreDataKit initialized ok
    func testCoreDataStack() {
        let app = UIApplication.sharedApplication()
        let delegate = app.delegate as? AppDelegate
        guard let stack = delegate?.stack else {
            XCTFail("Core Data stack does not exist")
            return
        }

        XCTAssertNotNil(stack.model.storeURL, " bad model store")
        XCTAssertNotNil(stack.model.managedObjectModel, " bad model")
        XCTAssertNotNil(stack.mainContext, "bad main MOC")
        XCTAssertNotNil(stack.backgroundContext, "bad background MOC")
        XCTAssertNotNil(stack.storeCoordinator, "bad PSC")
    }
}

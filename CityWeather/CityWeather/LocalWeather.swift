//
//  LocalWeather.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class LocalWeather: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	static let DataKey = "data"

    static let CityKey = "city"
    static let TimestampKey = "timestamp"


    // MARK: Properties
	@NSManaged var data: Data

    @NSManaged var city: String
    @NSManaged var timestamp: NSDate

    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        data = Data(json: json[LocalWeather.DataKey], context: context)
        data.localWeather = self
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[LocalWeather.DataKey] = data.dictionaryRepresentation()
        dictionary[LocalWeather.CityKey] = city
        dictionary[LocalWeather.TimestampKey] = timestamp

        return dictionary
    }
}

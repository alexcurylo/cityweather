//
//  DetailViewController.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-03.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import Crashlytics
import AlamofireImage

/**
 View of the 5 day forecast for a specified city
 */
final class DetailViewController: UIViewController, UIPageViewControllerDataSource {

    // MARK: - Properties

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var topLeftLabel: UILabel!
    @IBOutlet weak var topRightLabel: UILabel!
    @IBOutlet weak var midLeftLabel: UILabel!
    @IBOutlet weak var midRightLabel: UILabel!
    @IBOutlet weak var botLeftLabel: UILabel!
    @IBOutlet weak var botRightLabel: UILabel!

    weak var daysController: UIPageViewController!
    var days = [DayViewController]()
    var dayIndex = 0

    // MARK: - View model

    var viewModel: DetailViewModel? {
        didSet {
            guard let viewModel = viewModel else {
                return
            }

            viewModel.didUpdate = { [weak self] in
                self?.viewModelDidUpdate()
            }

            viewModelDidUpdate()
        }
    }

    // MARK: - Life cycle

    func viewModelDidUpdate() {
        guard let _ = backgroundImage, vm = viewModel else {
            return
        }

        topLeftLabel.text = vm.cityLabel
        topRightLabel.text = vm.timeLabel
        midLeftLabel.text = vm.temperatureLabel
        midRightLabel.text = vm.rainLabel
        botLeftLabel.text = vm.descriptionLabel
        botRightLabel.text = vm.windLabel
        if let imageURL = vm.imageURL {
            backgroundImage.af_setImageWithURL(imageURL)
        }

        for (index, day) in days.enumerate() {
            day.viewModel = vm.dayViewModel(atIndex: index)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModelDidUpdate()

        Answers.logContentViewWithName(String(self.dynamicType), contentType: nil, contentId: nil, customAttributes: nil)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let typedInfo = R.segue.detailViewController.embedPages(segue: segue) {
            daysController = typedInfo.destinationViewController
            for index in 0..<5 {
                if let day = R.storyboard.main.dayView() {
                    day.viewModel = viewModel?.dayViewModel(atIndex: index)
                    days.append(day)
                }
            }
            daysController.dataSource = self
            daysController.setViewControllers([days[0]], direction: .Forward, animated: false, completion: nil)

            }
    }

    // MARK: - Page Controller

    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        guard let day = viewController as? DayViewController else {
            return nil
        }

        let index = days.indexOf(day) ?? 0
        switch index {
        case 1..<days.count:
            dayIndex = index - 1
            return days[dayIndex]
        default:
            return nil
        }
    }

    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        guard let day = viewController as? DayViewController else {
            return nil
        }

        let index = days.indexOf(day) ?? Int.max
        switch index {
        case 0..<days.count - 1:
            dayIndex = index + 1
            return days[dayIndex]
        default:
            return nil
        }
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return days.count
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return dayIndex
    }
}

//
//  ModelTests.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-05.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import XCTest
@testable import CityWeather
import JSQCoreDataKit

class ModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testLocalWeatherModel() {
        let app = UIApplication.sharedApplication()
        let delegate = app.delegate as? AppDelegate
        guard let stack = delegate?.stack else {
            XCTFail("Core Data stack does not exist")
            return
        }

        var modelEntity = entity(name: "Astronomy", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Astronomy")
        modelEntity = entity(name: "ClimateAverages", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad ClimateAverages")
        modelEntity = entity(name: "CurrentCondition", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad CurrentCondition")
        modelEntity = entity(name: "Data", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Data")
        modelEntity = entity(name: "Hourly", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Hourly")
        modelEntity = entity(name: "LocalWeather", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad LocalWeather")
        modelEntity = entity(name: "Month", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Month")
        modelEntity = entity(name: "Request", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Request")
        modelEntity = entity(name: "Weather", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Weather")
        modelEntity = entity(name: "WeatherDesc", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad WeatherDesc")
        modelEntity = entity(name: "WeatherIconUrl", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad WeatherIconUrl")
    }

    func testLocationSearchModel() {
        let app = UIApplication.sharedApplication()
        let delegate = app.delegate as? AppDelegate
        guard let stack = delegate?.stack else {
            XCTFail("Core Data stack does not exist")
            return
        }

        var modelEntity = entity(name: "AreaName", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad AreaName")
        modelEntity = entity(name: "Country", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Country")
        modelEntity = entity(name: "LocationSearch", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad LocationSearch")
        modelEntity = entity(name: "Region", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Region")
        modelEntity = entity(name: "Result", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad Result")
        modelEntity = entity(name: "SearchApi", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad SearchApi")
        modelEntity = entity(name: "WeatherUrl", context: stack.mainContext)
        XCTAssertNotNil(modelEntity, "bad WeatherUrl")
    }
}

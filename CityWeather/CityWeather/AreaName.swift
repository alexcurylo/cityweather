//
//  AreaName.swift
//
//  Created by Alex Curylo on 2016-06-05
//  Copyright (c) Trollwerks Inc. All rights reserved.
//

import CoreData
import SwiftyJSON
import JSQCoreDataKit

final class AreaName: NSManagedObject {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    static let ValueKey = "value"


    // MARK: Properties
    @NSManaged var result: Result

    @NSManaged var value: String


    // MARK: SwiftyJSON Initalizers
    convenience init(json: JSON, context: NSManagedObjectContext) {
        let entity = JSQCoreDataKit.entity(name: String(self.dynamicType), context: context)
        self.init(entity: entity, insertIntoManagedObjectContext: context)

        value = json[AreaName.ValueKey].stringValue
    }


    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    func dictionaryRepresentation() -> [String: AnyObject] {
        var dictionary = [String: AnyObject]()

        dictionary[AreaName.ValueKey] = value

        return dictionary
    }
}

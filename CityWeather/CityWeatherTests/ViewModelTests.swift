//
//  ViewModelTests.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-07.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import XCTest
@testable import CityWeather
import JSQCoreDataKit

class ViewModelTests: XCTestCase {

// swiftlint:disable:next force_unwrapping
    var stack: CoreDataStack!
    var network: NetworkController!

    override func setUp() {
        super.setUp()

        if stack == nil {
            let inMemoryModel = CoreDataModel(name: "CityWeather", bundle: _R.hostingBundle, storeType: .inMemory)
            let factory = CoreDataStackFactory(model: inMemoryModel)
            factory.createStack(onQueue: nil) { result in
                switch result {
                case .success(let stack):
                    self.stack = stack
                    self.network = NetworkController(stack: stack)
                case .failure(let error):
                    print("Memory model FAIL: \(error)")
                }
            }
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testMasterViewModel() {
        let master = MasterViewModel(stack: stack, network: network)

        XCTAssertEqual(master.cityCount, 0, "Unexpected cities")
        XCTAssertEqual(master.cityName(atIndex: 999), "", "Unexpected city name")
    }

    func testDetailViewModel() {
        let master = MasterViewModel(stack: stack, network: network)
        let detail = master.detailViewModel(atIndex: 0)

        XCTAssertEqual(detail.cityLabel, "", "Unexpected city label")
        let smallKitteh = NSURL(string: "http://placekitten.com/320/220")
        XCTAssertEqualOptional(a: detail.imageURL, b: smallKitteh, "Unexpected default URL")
    }

    func testDayViewModel() {
        let master = MasterViewModel(stack: stack, network: network)
        let detail = master.detailViewModel(atIndex: 0)
        let day = detail.dayViewModel(atIndex: 0)

        XCTAssertEqual(day.dateLabel, "", "Unexpected date label")
        let bigKitteh = NSURL(string: "http://placekitten.com/500/500")
        XCTAssertEqualOptional(a: day.imageURL, b: bigKitteh, "Unexpected default URL")
    }
}

//
//  MasterViewModel.swift
//  CityWeather
//
//  Created by Alex Curylo on 2016-06-06.
//  Copyright © 2016 Trollwerks Inc. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

/**
 View model of the list of locations weather can be retrieved for

 - note: NSFetchedResultsControllerDelegate requires a class
 */
class MasterViewModel: NSObject, NSFetchedResultsControllerDelegate {

    // MARK: - Model injections

    private(set) var stack: CoreDataStack? {
        didSet {
            fetch()
        }
    }
    private(set) var network: NetworkController?

    convenience init(stack: CoreDataStack, network: NetworkController) {
        self.init()

        // defer to trigger didSet
        defer {
            self.stack = stack
            self.network = network
        }
    }

    // MARK: - View callbacks

    var didUpdate: (() -> Void)?
    var didError: ((NSError) -> Void)?
    var willSelect: (([String], (String) -> Void) -> Void)?

    // MARK: - View accessors

    var cityCount: Int {
        get {
            let sectionInfo = fetchedResultsController?.sections?[0]
            return sectionInfo?.numberOfObjects ?? 0
        }
    }

    func cityName(atIndex index: Int) -> String {
        guard let city = localWeather(atIndex: index)?.city else {
            return ""
        }
        return city
    }

    func deleteCity(atIndex index: Int) {
        guard let context = fetchedResultsController?.managedObjectContext,
            localWeather = localWeather(atIndex: index) else {
            return
        }

        context.deleteObject(localWeather)
        saveContext(context) { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                // Surprising but not fatal in release builds
                assertionFailure("Failed to save context: \(error)")
            }
        }
    }

    func addCity(named city: String, completion: NetworkCompletion? = nil) {
        guard let (mainContext, fetchRequest) = mainLocalWeatherFetchRequest() else {
            completion != nil ? completion?(.Failure(error())) : didError?(error())
            return
        }

        let predicate = NSPredicate(format:"city LIKE %@", city)
        fetchRequest.predicate = predicate
        var results = [LocalWeather]()
        do {
            results = try mainContext.fetch(request: fetchRequest)
        } catch {
            // Surprising but not fatal in release builds
            assertionFailure("FETCH FAIL: \(error)")
        }
        guard results.isEmpty else {
            let exists = error(R.string.localizable.errorCityExists())
            completion != nil ? completion?(.Failure(exists)) : didError?(exists)
            return
        }

        network?.locationSearch(city) { [weak self] result in
            switch result {
            case .Available, .Success:
                self?.findCity(city, completion: completion)
                break
            default:
                completion?(result)
                return
           }
        }
    }

    func findCity(query: String, completion: NetworkCompletion?) {
        guard let mainContext = stack?.mainContext,
            queries = network?.existingQueries(query, context: mainContext) where !queries.isEmpty else {
            completion?(.Failure(error()))
            return
        }

        for query in queries {
            guard let results = query.searchApi.result.array as? [Result] else {
                continue
            }

            let names: [String] = results.flatMap {
                let name  = $0.areaName.firstObject?.value ?? ""
                let country  = $0.country.firstObject?.value ?? ""
                return country.isEmpty ? name : "\(name), \(country)"
            }
            switch names.count {
            case 0:
                continue
            case 1:
                network?.getLocalWeather(names[0], completion: completion)
                return
            default:
                guard let willSelect = willSelect else {
                    network?.getLocalWeather(names[0], completion: completion)
                    return
                }
                completion?(.Success)
                willSelect(names) { [weak self] city in
                    guard !city.isEmpty else {
                        return
                    }
                    self?.network?.getLocalWeather(city) { [weak self] result in
                        if case .Failure(let error) = result {
                            self?.didError?(error)
                        }
                    }
                }
                return
            }
        }
        completion?(.Failure(error()))
    }

    func detailViewModel(atIndex index: Int) -> DetailViewModel {
        guard let localWeather = localWeather(atIndex:index) else {
            return DetailViewModel(localWeather: [:])
        }

        // Updates, if any are needed, will silently refresh on success
        network?.getLocalWeather(localWeather.city)

        return DetailViewModel(localWeather: localWeather.dictionaryRepresentation())
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController? {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                self.didUpdate?()
            }
        }
    }

    func fetch() {
        guard let (mainContext, fetchRequest) = mainLocalWeatherFetchRequest() else {
            return
        }

        let sortDescriptor = NSSortDescriptor(key: LocalWeather.CityKey, ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]

        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: mainContext, sectionNameKeyPath: nil, cacheName: String(self.dynamicType))
        aFetchedResultsController.delegate = self

        do {
            try aFetchedResultsController.performFetch()
        } catch let error as NSError {
            // App is non-functional. Crash.
            fatalError("FRC FAIL: \(error)")
        }

        fetchedResultsController = aFetchedResultsController
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        didUpdate?()
    }

    func mainLocalWeatherFetchRequest() -> (NSManagedObjectContext, FetchRequest<LocalWeather>)? {
        guard let mainContext = stack?.mainContext else {
            return nil
        }

        let lwEntity = entity(name: String(LocalWeather), context: mainContext)
        let fetchRequest = FetchRequest<LocalWeather>(entity: lwEntity)
        fetchRequest.fetchBatchSize = 20
        return (mainContext, fetchRequest)
    }

    func localWeather(atIndex index: Int) -> LocalWeather? {
        let indexPath = NSIndexPath(forRow: index, inSection: 0)
        guard let frc = fetchedResultsController where frc.fetchedObjects?.count ?? 0 > index else {
                return nil
        }
        return frc.objectAtIndexPath(indexPath) as? LocalWeather
    }

    // MARK: - Error generator convenience

    func error(message: String? = nil) -> NSError {
        let message = message ?? R.string.localizable.errorUnexpected()
        let error = NSError(domain: "Model", code: 1, userInfo: [NSLocalizedDescriptionKey: message])
        return error
    }
}
